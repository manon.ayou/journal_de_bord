from logging import debug
from flask import Flask, g, render_template, request
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy

from flask_wtf import FlaskForm,Form
from wtforms import SubmitField, SelectField, RadioField, HiddenField, StringField, IntegerField, FloatField, TextField, BooleanField, PasswordField, TextAreaField, validators,Form, BooleanField, StringField, PasswordField
from wtforms.validators import InputRequired, Length, Regexp, NumberRange
from datetime import date

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import os
import sqlite3
engine = create_engine('sqlite:////tmp/test.db')
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()




app = Flask(__name__, template_folder='../templates')
current_directory = os.path.dirname(os.path.abspath(__file__))

app.config['SECRET_KEY'] = 'Pifc4Amdjpld7Sc2Jsug4-MF88'

db_name = 'database.db'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_name

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)


def connect_db():
    sql = sqlite3.connect('../database/database.db')
    sql.row_factory = sqlite3.Row

    return sql


def get_db():
    if not hasattr(g, 'sqlite3'):
        g.sqlite3_db = connect_db()
    return g.sqlite3_db


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'sqlite3'):
        g.sqlite3_db.close()


@app.route('/')
def login():
    return render_template('login.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = RegistrationForm(form.username.data, form.email.data,
                    form.password.data)
        db_session.add(user)
        print('Thanks for registering')
    return render_template('home.html', form=form)




@app.route('/home',methods=['POST'])
def home():
    return render_template('home.html')


@app.route('/users/')
def viewusers():
    db = get_db()
    cursor = db.execute('SELECT learner,body,attitude,created FROM report')
    results = cursor.fetchall()
    return f" Apprenant.e {results[0]['learner']}. <br> Attitude: {results[0]['attitude']}.<br>  Constatation : {results[0]['body']}.<br><br>  Rapport envoyé le {results[0]['created']}."





class RegistrationForm(Form):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the TOS', [validators.DataRequired()])





if __name__ == '__main__':
    app.run(debug=True)
